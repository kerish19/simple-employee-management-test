<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\City;
use App\Country;
use App\State;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call('TableSeeder');
        $this->command->info('Tables seeded!');
    }
}

class TableSeeder extends Seeder 
{

    public function run()
    {
        User::create(['username' => 'paul', 'lastname' => 'gatchalian', 'firstname' => 'Paul', 'email' => 'paulkervingatchalian@gmail.com','password' => Hash::make('admin')]);
        
        Country::create(['country_code' => 'PH', 'name' => 'Philippines']);
        Country::create(['country_code' => 'US', 'name' => 'United States of America']);
        
        State::create(['country' => '1', 'name' => 'NCR']);
        State::create(['country' => '1', 'name' => 'Quezon']);
        
        City::create(['state' => '1', 'name' => 'Quezon City']);
        City::create(['state' => '2', 'name' => 'Tayabas City']);
        
    }

}
