<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lastname',60);
            $table->string('firstname',60);
            $table->string('middlename',60);
            $table->string('address',120)->nullable();
            $table->integer('city')->default(0);
            $table->foreign('city')->references('id')->on('city');
            $table->integer('state')->default(0);
            $table->foreign('state')->references('id')->on('state');
            $table->integer('country')->default(0);
            $table->foreign('country')->references('id')->on('country');
            $table->char('zip',10)->nullable();
            $table->integer('age')->nullable();
            $table->date('birthdate')->nullable();
            $table->date('date_hired')->nullable();
            $table->integer('department')->nullable();
            $table->foreign('department')->references('id')->on('department');
            $table->integer('division')->nullable();
            $table->foreign('division')->references('id')->on('division');
            $table->integer('company')->nullable();
            $table->string('picture',60)->nullable();
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
