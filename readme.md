
## Setup Guide

- Download/Clone the repository to local dev
- Update **.env** file to suit local database credentials
- Use composer and run **"composer update"** to initiate the installation of all dependencies into local dev.
- Run **"php artisan migrate:refresh --seed"** to initiate all database migrations and seeds

## Sample User Account
- Username/Email: **paulkervingatchalian@gmail.com**
- Password: **admin**