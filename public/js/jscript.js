(function($){

$.appjs = {
    init : {
        main: function()
        {
                $("#search").on("click", function(e) {
                    e.preventDefault(), $(".search-box").fadeIn()
                }), $(".dismiss").on("click", function() {
                    $(".search-box").fadeOut()
                }), $(".card-close a.remove").on("click", function(e) {
                    e.preventDefault(), $(this).parents(".card").fadeOut()
                }), $('[data-toggle="tooltip"]').tooltip(), $(".dropdown").on("show.bs.dropdown", function() {
                    $(this).find(".dropdown-menu").first().stop(!0, !0).fadeIn()
                }), $(".dropdown").on("hide.bs.dropdown", function() {
                    $(this).find(".dropdown-menu").first().stop(!0, !0).fadeOut()
                }), $("#toggle-btn").on("click", function(e) {
                    e.preventDefault(), $(this).toggleClass("active"), $(".side-navbar").toggleClass("shrinked"), $(".content-inner").toggleClass("active"), $(document).trigger("sidebarChanged"), $(window).outerWidth() > 1183 && ($("#toggle-btn").hasClass("active") ? ($(".navbar-header .brand-small").hide(), $(".navbar-header .brand-big").show()) : ($(".navbar-header .brand-small").show(), $(".navbar-header .brand-big").hide())), $(window).outerWidth() < 1183 && $(".navbar-header .brand-small").show()
                }), $(".form-validate").each(function() {
                    $(this).validate({
                        errorElement: "div",
                        errorClass: "is-invalid",
                        validClass: "is-valid",
                        ignore: ":hidden:not(.summernote, .checkbox-template, .form-control-custom),.note-editable.card-block",
                        errorPlacement: function(e, t) {
                            e.addClass("invalid-feedback"), console.log(t), "checkbox" === t.prop("type") ? e.insertAfter(t.siblings("label")) : e.insertAfter(t)
                        }
                    })
                });
            
                $(document).on("sidebarChanged", function() {
                }), $(window).on("resize", function() {
                }), $(".external").on("click", function(e) {
                    e.preventDefault(), window.open($(this).attr("href"))
                });
                
                $.ajaxSetup({
                    headers: {
                      'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                    }
                });
                
                $('.numeric').keyup(function(e)
                {
                  if (/\D/g.test(this.value))
                  {
                    this.value = this.value.replace(/\D/g, '');
                  }
                }); 

        },
        custom: function(options)
        {		
    			var settings = $.extend({}, 
                {
                    delete_url: null
    			}, options);
                
                $('.photo').on('click',function()
                {
                    $('.picture').click();
                });
                
                $('#datepickerto').datepicker({
                    uiLibrary: 'bootstrap4',
                    format: 'yyyy-dd-mm'
                });
                $('#datepickerfrom').datepicker({
                    uiLibrary: 'bootstrap4',
                    format: 'yyyy-dd-mm'
                });
                
                $(document).on('click','.delete-btn',function(e)
                {
                        var theid = $(this).data('id');
                        var thetbl = $(this).data('table');
                        if(confirm("Are you sure you want to remove this record?"))
                        {
                            $.ajax({
                                    method: 'POST',
                                    url: settings.delete_url,
                                    data: {table: thetbl, id: theid}
                                }).done(function(msg)
                                {
                                        alert(msg);
                                        window.location.reload();
                                });
                        }
                        e.preventDefault();
                });
                
                $(document).on('click','.deleted',function(e)
                {
                        var theurl = $(this).data('url');
                        if(confirm("Are you sure you want to remove this record?"))
                        {
                            $.ajax({
                                    type: 'DELETE',
                                    url: theurl
                                }).done(function(msg)
                                {
                                        alert(msg);
                                        window.location.reload();
                                });
                        }
                        e.preventDefault();
                });
                $(document).on('click','.dlbtn',function(e)
                {
                    var theurl = $(this).data('url');
                    var dfrom = $('#datepickerfrom').val();
                    var dto = $('#datepickerto').val();
                    if($(this).hasClass('withdates'))
                    {
                        if(dfrom=='' || dto=='')
                        {
                            alert('Please supply the dates!');
                        }else{
                            window.location.href = theurl+'?datefrom='+dfrom+'&dateto='+dto;
                        }
                    }else{
                        window.location.href = theurl;
                    }
                    e.preventDefault();
                });

       },
       formvalidation: function()
       {
            $(document).ready(function()
            {
                $(".forms").validate();
                $(".forms2").validate();
                $(".required").rules("add", { 
                  required:true
                });
            });
       }
    }
}

})(jQuery);	