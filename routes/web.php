<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// login routes
Route::get('/', array('as' => 'login', 'uses' => 'LoginController@viewForm'));
Route::post('login', array('uses' => 'LoginController@doLogin'));


// admin routes
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function()
{
    
    Route::get('logout',array('uses' => 'AdminController@logout'));
    
    //dashboard
    Route::get('dashboard',array('uses' => 'AdminController@dashboard'));
    
    // employees management
    Route::resource('employees', 'EmployeeController', ['names' => [
        'update' => 'admin.employees.update']
    ])->except(['destroy']);
    
    // employee salary management
    Route::resource('employees-salary', 'EmployeesSalaryController', ['names' => [
        'update' => 'admin.employeesalary.update']
    ])->except(['destroy']);
    
    // global delete records
    Route::post('deleterecords',array('uses' => 'AdminController@deleteRecords'));
    
    // users management
    Route::resource('users', 'UsersController', ['names' => [
        'update' => 'admin.users.update']
    ])->except(['destroy']);
    
    //update user password
    Route::post('users/password/update',array('uses' => 'UsersController@updatePassword'));
    
    
    // systems management
    Route::resource('system/department', 'DepartmentController', ['names' => [
        'update' => 'admin.department.update']
    ]);
    Route::resource('system/division', 'DivisionController', ['names' => [
        'update' => 'admin.division.update']
    ]);
    Route::resource('system/country', 'CountryController', ['names' => [
        'update' => 'admin.country.update']
    ]);
    Route::resource('system/state', 'StateController', ['names' => [
        'update' => 'admin.state.update']
    ]);
    Route::resource('system/city', 'CityController', ['names' => [
        'update' => 'admin.city.update']
    ]);
    
    
    //search filters
    Route::post('search/employees',array('uses' => 'EmployeeController@index'));
    Route::post('search/employeesalary',array('uses' => 'EmployeesSalaryController@index'));
    Route::post('search/users',array('uses' => 'UsersController@index'));
    Route::post('search/department',array('uses' => 'DepartmentController@index'));
    Route::post('search/division',array('uses' => 'DivisionController@index'));
    Route::post('search/country',array('uses' => 'CountryController@index'));
    Route::post('search/state',array('uses' => 'StateController@index'));
    Route::post('search/city',array('uses' => 'CityController@index'));
    
    
    //reports
    Route::get('reports',array('uses' => 'AdminController@reports'));
    Route::get('reports/hired/excel',array('as' => 'admin.reportshired.excel', 'uses' => 'ReportController@hiredExcel'));
    Route::get('reports/hired/pdf',array('as' => 'admin.reportshired.pdf', 'uses' => 'ReportController@hiredPDF'));
    
    Route::get('reports/salary/excel',array('as' => 'admin.reportsalary.excel', 'uses' => 'ReportController@salaryExcel'));
    Route::get('reports/salary/pdf',array('as' => 'admin.reportsalary.pdf', 'uses' => 'ReportController@salaryPDF'));
    
});