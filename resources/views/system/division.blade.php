@extends('layouts.app')
@section('content')

<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Divisions</h2>
              <a href="{{ url('admin/system/division/create') }}" class="btn btn-success pull-right" style="margin-top: -30px;"><i class="fa fa-plus"></i> Create New Division</a>

            </div>
</header>  

<div class="container" style="padding: 20px;">  

<div class="row">
<div class="col-md-12">
    <h4>Search Filter</h4>
    <form name="searchform" method="post" action="{{ url('admin/search/division') }}">
    {{ csrf_field() }}
    <ul class="search-filter-box">
        <li>
            <div class="col-12 col-md-12 form-group">
                <label class="form-control-label">Division Name</label>
                <input type="text" name="division" class="form-control" />
            </div>
        </li>
        <li>
            <div class="col-12 col-md-12 form-group">
            <input type="submit" value="Search" name="search_btn" class="btn btn-info" />
             <input type="submit" value="Clear" name="search_btn" class="btn btn-warning" />
            </div>
        </li>
    </ul>
    </form>
</div>
</div>
<div class="table-responsive" style="margin-top: 20px;">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th></th>
                            </tr>
                          </thead>
<tbody>
@if(count($dep)>0)
@foreach($dep as $depp)
<tr>
<td>{{ $depp->id }}</td>
<td>{{ $depp->name }}</td>
<td>
<a href="{{ url('admin/system/division/'.$depp->id) }}" class="btn btn-sm btn-primary">Edit</a>
<a href="#" class="btn btn-sm btn-danger deleted" data-url="{{ url('admin/system/division/'.$depp->id) }}" >Delete</a>
</td>
</tr>
@endforeach
@else
<tr>
<td colspan="4" class="text-center">No record found in database.
</td>
</tr>
@endif
</tbody>
</table>
{!! $dep->render() !!}
                        
                      </div>
</div> 
@endsection