@extends('layouts.app')
@section('content')

<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">{{ $title }}</h2>
            </div>
</header>  

<div class="container" style="padding: 20px;">  

@if (Session::has('msg'))
<div class="alert alert-success">
    <p>{!! Session::get('msg') !!}</p>
</div>
@endif
      

@if(!empty($id))    
<form name="add_new_city" class="forms" action="{{ route('admin.city.update', $id) }}" method="post">
{{ method_field('PATCH') }}
@else
<form name="add_new_city" class="forms" action="{{ url('admin/system/city') }}" method="post">
@endif

{{ csrf_field() }}

<div class="col-12 col-md-6 form-group">
    <label class="form-control-label">Name</label>
    <input type="text" name="name" class="form-control required" value="{{ $info->name }}">
</div>
<div class="col-12 col-md-6 form-group">
    <label class="form-control-label">State</label>
    <select name="state" class="form-control required">
    <option value="">Select</option>
        @foreach($states as $state)
            @if($info->state==$state->id)
                @php ($selected='selected')
            @else
                @php ($selected='')
            @endif
        <option value="{{ $state->id }}" {{ $selected }}>{{ $state->name }}</option>
        @endforeach
    </select>
</div>
<div class="col-12 col-md-6 form-group">
<input type="submit" value="SAVE" name="add_btn" class="btn btn-lg btn-success" />
</div>
</form>
</div> 
@endsection