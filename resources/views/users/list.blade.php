@extends('layouts.app')
@section('content')

<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Users Management</h2>
              <a href="{{ url('admin/users/create') }}" class="btn btn-success pull-right" style="margin-top: -30px;"><i class="fa fa-plus"></i> Create New User</a>
            </div>
</header>  

<div class="container" style="padding: 20px;">  

<div class="row">
<div class="col-md-12">
    <h4>Search Filter</h4>
    <form name="searchform" method="post" action="{{ url('admin/search/users') }}">
    {{ csrf_field() }}
    <ul class="search-filter-box">
        <li>
            <div class="col-12 col-md-12 form-group">
                <label class="form-control-label">Firstname</label>
                <input type="text" name="firstname" class="form-control" />
            </div>
        </li>
        <li>
            <div class="col-12 col-md-12 form-group">
                <label class="form-control-label">Lastname</label>
                <input type="text" name="lastname" class="form-control" />
            </div>
        </li>
        <li>
            <div class="col-12 col-md-12 form-group">
                <label class="form-control-label">Username/Email</label>
                <input type="text" name="email" class="form-control" />
            </div>
        </li>
        <li>
            <div class="col-12 col-md-12 form-group">
            <input type="submit" value="Search" name="search_btn" class="btn btn-info" />
             <input type="submit" value="Clear" name="search_btn" class="btn btn-warning" />
            </div>
        </li>
    </ul>
    </form>
</div>
</div>

<div class="table-responsive" style="margin-top: 20px;">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Username/Email</th>
                              <th></th>
                            </tr>
                          </thead>
<tbody>
@if(count($users)>0)
@foreach($users as $use)
<tr>
<td>{{ $use->id }}</td>
<td>{{ $use->firstname.' '.$use->lastname }}</td>
<td>{{ $use->email }}</td>
<td>
<a href="{{ url('admin/users/'.$use->id) }}" class="btn btn-sm btn-primary">Edit</a>
<a href="#" class="btn btn-sm btn-danger delete-btn" data-id={{ $use->id }} data-table="users">Delete</a>
</td>
</tr>
@endforeach
@else
<tr>
<td colspan="4" class="text-center">No record found in database.
</td>
</tr>
@endif
</tbody>
</table>
{!! $users->render() !!}
                        
                      </div>
</div> 
@endsection