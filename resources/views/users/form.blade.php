@extends('layouts.app')
@section('content')

<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">{{ $title }}</h2>
            </div>
</header>  

<div class="container" style="padding: 20px;">  

@if (Session::has('msg'))
<div class="alert alert-success">
    <p>{!! Session::get('msg') !!}</p>
</div>
@endif
      
      
<div class="row">

@if(!empty($id)) 
<div class="col-md-7">    
<form name="add_new_user" class="forms" action="{{ route('admin.users.update', $id) }}" method="post">
{{ method_field('PATCH') }}
@else
<div class="col-md-12">
<form name="add_new_user" class="forms" action="{{ url('admin/users') }}" method="post">
@endif 
{{ csrf_field() }}

<div class="row">
<div class="col-12 col-md-6 form-group">
    <label class="form-control-label">First Name</label>
    <input type="text" name="firstname" class="form-control required" value="{{ $use->firstname }}">
</div>
<div class="col-12 col-md-6 form-group">
    <label class="form-control-label">Last Name</label>
    <input type="text" name="lastname" class="form-control required" value="{{ $use->lastname }}">
</div>
</div>

<div class="row">
<div class="col-12 col-md-6 form-group">
    <label class="form-control-label">Username/Email</label>
    <input type="text" name="email" class="form-control required" value="{{ $use->email }}">
</div>
@if(empty($id)) 
<div class="col-12 col-md-6 form-group">
    <label class="form-control-label">Password</label>
    <input type="text" name="password" class="form-control required">
</div>
@endif
</div>

<hr />
<input type="submit" value="SAVE" name="add_btn" class="btn btn-lg btn-success" />
</form>
</div>

@if(!empty($id)) 
<div class="col-md-5" style="background: white;padding: 20px;">
<div class="col-md-12">
<h3>Update Password</h3>
</div>

@if (Session::has('msg-error'))
<div class="col-md-12">
<div class="alert alert-danger">
    <p>{!! Session::get('msg-error') !!}</p>
</div>
</div>
@endif

@if (Session::has('msg-success'))
<div class="col-md-12">
<div class="alert alert-success">
    <p>{!! Session::get('msg-success') !!}</p>
</div>
</div>
@endif

<form name="update_password" method="post" class="forms2" action="{{ url('admin/users/password/update') }}">
<input type="hidden" name="id" value="{{ $id }}" />
{{ csrf_field() }}
<div class="col-md-12 form-group">
    <label class="form-control-label">Current Password</label>
    <input type="text" name="oldpass" class="form-control required">
</div>
<div class="col-md-12 form-group">
    <label class="form-control-label">New Password</label>
    <input type="text" name="password" class="form-control required">
</div>
<div class="col-md-12 form-group">
    <label class="form-control-label">Re-enter Password</label>
    <input type="text" name="password2" class="form-control required">
</div>
<div class="col-md-12">
<input type="submit" value="UPDATE" name="update_btn" class="btn btn-lg btn-success" />
</div>
</form>
</div>
@endif

</div>

</div>
@endsection