@extends('layouts.app')
@section('content')

<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Reports</h2>
            </div>
</header>  

<div class="container" style="padding: 20px;">       
<table class="table">
    <tr>
        <td>List of employees with date range using date hired.</td>
        <td>
        <div class="row">
            <div class="col-md-6">
            <label>From</label>
            <input type="text" name="datefrom" class="form-control" id="datepickerfrom" />
            </div>
            <div class="col-md-6">
            <label>To</label>
            <input type="text" name="dateto" class="form-control" id="datepickerto" />
            </div>
        </div>
        <br />
        <a href="#" data-url="{{ route('admin.reportshired.excel') }}" class="btn btn-success dlbtn withdates">Export to Excel</a>
        <a href="#" data-url="{{ route('admin.reportshired.pdf') }}" class="btn btn-warning dlbtn withdates">Export to Pdf</a>
        </td>
    </tr>
    <tr>
    <td>List of employees sorted by salary</td>
    <td> <a href="#" data-url="{{ route('admin.reportsalary.excel') }}" class="btn btn-success dlbtn">Export to Excel</a>
        <a href="#" data-url="{{ route('admin.reportsalary.pdf') }}" class="btn btn-warning dlbtn">Export to Pdf</a></td>
    </tr>
</table>
</div>
@endsection