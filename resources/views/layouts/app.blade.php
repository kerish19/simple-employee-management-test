<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Simple Employee Management</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <meta name="_token" content="{{ csrf_token() }}">
    
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://d19m59y37dris4.cloudfront.net/admin/1-4-4/vendor/font-awesome/css/font-awesome.min.css">

    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" id="theme-stylesheet">
    
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    
    <link rel="stylesheet" href="{{ asset('css/datetimepicker.min.css') }}">

    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  </head>
  <body>
  
    <div class="page">
    
      <!-- Main Navbar-->
      <header class="header">
        <nav class="navbar">

          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <!-- Navbar Header-->
              <div class="navbar-header">
                <!-- Navbar Brand --><a href="{{ url('admin/dashboard') }}" class="navbar-brand d-none d-sm-inline-block">
                  <div class="brand-text d-none d-lg-inline-block"><strong>Dashboard</strong></div>
                  <div class="brand-text d-none d-sm-inline-block d-lg-none"><strong>BD</strong></div></a>
                <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
              </div>
              <!-- Navbar Menu -->
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
               <li class="nav-item d-flex align-items-center">Welcome {{ $user }}</li>
                    <li class="nav-item"><a href="{{ url('admin/logout') }}" class="nav-link logout"> <span class="d-none d-sm-inline">Logout</span><i class="fa fa-sign-out"></i></a></li>
          </ul>
          
          </div>
          </div>
      </nav>
    </header>



<div class="page-content d-flex align-items-stretch"> 

        <!-- Side Navbar -->
        <nav class="side-navbar">
        
          <!-- Sidebar Navidation Menus-->
          <ul class="list-unstyled">
                    <li class="active"><a href="{{ url('admin/dashboard') }}"> <i class="fa fa-dashboard"></i>Dashboard </a></li>
 
                    <li><a href="#exampledropdownDropdown0" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-users"></i>Employee Management </a>
                      <ul id="exampledropdownDropdown0" class="collapse list-unstyled ">
                        <li><a href="{{ url('admin/employees') }}"> <i class="fa fa-users"></i>Employees </a></li>
                         <li><a href="{{ url('admin/employees-salary') }}"> <i class="fa fa-money"></i>Employee Salary </a></li>
                      
                      </ul>
                    </li>
                    
                    
                    <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-cogs"></i>System Management </a>
                      <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                        <li><a href="{{ url('admin/system/department') }}"><i class="fa fa-cog"></i> Department</a></li>
                        <li><a href="{{ url('admin/system/division') }}"><i class="fa fa-cog"></i> Division</a></li>
                        <li><a href="{{ url('admin/system/country') }}"><i class="fa fa-cog"></i> Country</a></li>
                        <li><a href="{{ url('admin/system/state') }}"><i class="fa fa-cog"></i> State</a></li>
                        <li><a href="{{ url('admin/system/city') }}"><i class="fa fa-cog"></i> City</a></li>
                      </ul>
                    </li>
                    <li><a href="#exampledropdownDropdown2" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-user"></i>User Management </a>
                      <ul id="exampledropdownDropdown2" class="collapse list-unstyled ">
                        <li><a href="{{ url('admin/users') }}"><i class="fa fa-user"></i> User</a></li>
                      
                      </ul>
                    </li>
                    <li><a href="{{ url('admin/reports') }}"> <i class="fa fa-files-o"></i>Reports </a></li>
          </ul>
        </nav>
 
<!-- main inside container -->       
<div class="content-inner">
@yield('content')
</div>
<!-- end container -->

</div>

</div>

</body>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"> </script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.js') }}"></script>
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/datetimepicker.min.js') }}"></script>
<script src="{{ asset('js/jscript.js') }}"></script>

<script>
$(document).ready(function()
{
    $.appjs.init.main();
    $.appjs.init.custom({delete_url: "{{ url('admin/deleterecords') }}"});
    @if(!Request::is('admin/reports'))
        $.appjs.init.formvalidation();
    @endif
});
</script>
</html>
