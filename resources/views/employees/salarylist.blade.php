@extends('layouts.app')
@section('content')

<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Employee Salary Management</h2>
              <a href="{{ url('admin/employees-salary/create') }}" class="btn btn-success pull-right" style="margin-top: -30px;"><i class="fa fa-plus"></i> Create New Record</a>

            </div>
</header>  

<div class="container" style="padding: 20px;">  

<div class="row">
<div class="col-md-12">
    <h4>Search Filter</h4>
    <form name="searchform" method="post" action="{{ url('admin/search/employeesalary') }}">
    {{ csrf_field() }}
    <ul class="search-filter-box">
        <li>
            <div class="col-12 col-md-12 form-group">
                <label class="form-control-label">Employee Name</label>
                <input type="text" name="employee" class="form-control" />
            </div>
        </li>
        <li>
            <div class="col-12 col-md-12 form-group">
                <label class="form-control-label">Salary</label>
                <input type="text" name="salary" class="form-control" />
            </div>
        </li>
        <li>
            <div class="col-12 col-md-12 form-group">
            <input type="submit" value="Search" name="search_btn" class="btn btn-info" />
             <input type="submit" value="Clear" name="search_btn" class="btn btn-warning" />
            </div>
        </li>
    </ul>
    </form>
</div>
</div>


<div class="table-responsive" style="margin-top: 20px;">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Employee Name</th>
                              <th>Salary</th>
                              <th></th>
                            </tr>
                          </thead>
<tbody>
@if(count($employees)>0)
@foreach($employees as $employee)
<tr>
<td>{{ $employee->id }}</td>
<td>{{ $employee->name }}</td>
<td>{{ $employee->salary }}</td>
<td>
<a href="{{ url('admin/employees-salary/'.$employee->id) }}" class="btn btn-sm btn-primary">Edit</a>
<a href="#" class="btn btn-sm btn-danger delete-btn" data-id={{ $employee->id }} data-table="employee_salary">Delete</a>
</td>
</tr>
@endforeach
@else
<tr>
<td colspan="4" class="text-center">No record found in database.
</td>
</tr>
@endif
</tbody>
</table>
{!! $employees->render() !!}
                        
                      </div>
</div> 
@endsection