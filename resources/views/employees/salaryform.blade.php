@extends('layouts.app')
@section('content')

<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">{{ $title }}</h2>
            </div>
</header>  

<div class="container" style="padding: 20px;">  

@if (Session::has('msg'))
<div class="alert alert-success">
    <p>{!! Session::get('msg') !!}</p>
</div>
@endif
      
@if(!empty($id))     
<form name="add_new_employee_salary" class="forms" action="{{ route('admin.employeesalary.update', $id) }}" method="post" enctype="multipart/form-data">
{{ method_field('PATCH') }}
@else
<form name="add_new_employee_salary" class="forms" action="{{ url('admin/employees-salary') }}" method="post" enctype="multipart/form-data">
@endif

{{ csrf_field() }}


<div class="col-12 col-md-6 form-group">
    <label class="form-control-label">Employee</label>
    <select name="employee" class="form-control required">
    <option value="">Select</option>
        @foreach($employees as $emp)
            @if($employee->employee==$emp->id)
                @php ($selected='selected')
            @else
                @php ($selected='')
            @endif
        <option value="{{ $emp->id }}" {{ $selected }}>{{ $emp->firstname.' '.$emp->middlename.' '.$emp->lastname }}</option>
        @endforeach
    </select>
</div>
<div class="col-12 col-md-6 form-group">
    <label class="form-control-label">Salary</label>
    <input type="text" name="salary" class="form-control required" value="{{ $employee->salary }}">
</div>
<div class="col-12 col-md-6 form-group">
<input type="submit" value="SAVE" name="add_btn" class="btn btn-lg btn-success" />
</div>
</form>
</div>
@endsection