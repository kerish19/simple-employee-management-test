@extends('layouts.app')
@section('content')

<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">{{ $title }}</h2>
            </div>
</header>  

<div class="container" style="padding: 20px;">  

@if (Session::has('msg'))
<div class="alert alert-success">
    <p>{!! Session::get('msg') !!}</p>
</div>
@endif
      
@if(!empty($id))     
<form name="add_new_employee" class="forms" action="{{ route('admin.employees.update', $id) }}" method="post" enctype="multipart/form-data">
{{ method_field('PATCH') }}
@else
<form name="add_new_employee" class="forms" action="{{ url('admin/employees') }}" method="post" enctype="multipart/form-data">
@endif

{{ csrf_field() }}


<div class="row">
<div class="col-md-9">
<div class="row">
<div class="col-12 col-md-4 form-group">
    <label class="form-control-label">First Name</label>
    <input type="text" name="firstname" class="form-control required" value="{{ $employee->firstname }}">
</div>
<div class="col-12 col-md-4 form-group">
    <label class="form-control-label">Middle Name</label>
    <input type="text" name="middlename" class="form-control required" value="{{ $employee->middlename }}">
</div>
<div class="col-12 col-md-4 form-group">
    <label class="form-control-label">Last Name</label>
    <input type="text" name="lastname" class="form-control required" value="{{ $employee->lastname }}">
</div>
</div>

<div class="row">
<div class="col-12 col-md-4 form-group">
    <label class="form-control-label">Address</label>
    <input type="text" name="address" class="form-control" value="{{ $employee->address }}">
</div>
<div class="col-12 col-md-4 form-group">
    <label class="form-control-label">City</label>
    <select name="city" class="form-control">
    <option value="0">Select</option>
        @foreach($cities as $city)
            @if($employee->city==$city->id)
                @php ($selected='selected')
            @else
                @php ($selected='')
            @endif
            <option value="{{ $city->id }}" {{ $selected }}>{{ $city->name }}</option>
        @endforeach
    </select>
</div>
<div class="col-12 col-md-4 form-group">
    <label class="form-control-label">State</label>
    <select name="state" class="form-control">
    <option value="0">Select</option>
        @foreach($states as $state)
            @if($employee->state==$state->id)
                @php ($selected='selected')
            @else
                @php ($selected='')
            @endif
        <option value="{{ $state->id }}" {{ $selected }}>{{ $state->name }}</option>
        @endforeach
    </select>
</div>
</div>


<div class="row">
<div class="col-12 col-md-4 form-group">
    <label class="form-control-label">Country</label>
    <select name="country" class="form-control">
    <option value="0">Select</option>
        @foreach($countries as $country)
            @if($employee->country==$country->id)
                @php ($selected='selected')
            @else
                @php ($selected='')
            @endif
        <option value="{{ $country->id }}" {{ $selected }}>{{ $country->country_code }}</option>
        @endforeach
    </select>
</div>
<div class="col-12 col-md-4 form-group">
    <label class="form-control-label">Zip</label>
    <input type="text" name="zip" class="form-control numeric">
</div>
<div class="col-12 col-md-4 form-group">
    <label class="form-control-label">Age</label>
    <input type="text" name="age" class="form-control numeric">
</div>
</div>


<div class="row">
<div class="col-12 col-md-4 form-group">
    <label class="form-control-label">Birt Date</label>
    <input type="text" name="birthdate" class="form-control date">
</div>
<div class="col-12 col-md-4 form-group">
    <label class="form-control-label">Date Hired</label>
    <input type="text" name="date_hired" class="form-control date">
</div>
<div class="col-12 col-md-4 form-group">
    <label class="form-control-label">Department</label>
    <select name="department" class="form-control">
    <option value="0">Select</option>
        @foreach($departments as $department)
            @if($employee->department==$department->id)
                @php ($selected='selected')
            @else
                @php ($selected='')
            @endif
        <option value="{{ $department->id }}" {{ $selected }}>{{ $department->name }}</option>
        @endforeach
    </select>
</div>
</div>

<div class="row">
<div class="col-12 col-md-4 form-group">
    <label class="form-control-label">Division</label>
    <select name="division" class="form-control">
    <option value="0">Select</option>
        @foreach($divisions as $division)
            @if($employee->division==$division->id)
                @php ($selected='selected')
            @else
                @php ($selected='')
            @endif
        <option value="{{ $division->id }}" {{ $selected }}>{{ $division->name }}</option>
        @endforeach
    </select>
</div>
<div class="col-12 col-md-4 form-group">
    <label class="form-control-label">Company</label>
    <input type="text" name="company" class="form-control">
</div>
</div>
</div>

<div class="col-md-3">
<div class="row">
<div class="col-12 form-group">
<label class="form-control-label">Picture</label>
</div>
<div class="col-12 col-md-4 form-group">
    @if(empty($employee->picture))
    <img src="{{ asset('images/upload.png') }}" class="img-fluid img-thumbnail photo" />
    <input type="file" name="picture" class="picture" style="margin-top: 10px;"/>
    @else
    <img src="{{ asset('files/'.$employee->picture) }}" class="img-fluid img-thumbnail photo" />
    <input type="file" name="picture" checked="picture" value="Replace" />
    @endif
</div>
</div>
</div>
</div>

<hr />
<input type="submit" value="SAVE" name="add_employee_btn" class="btn btn-lg btn-success" />
</form>
</div>
@endsection