<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Date Hired</th>
        <th>Address</th>
        <th>Age</th>
        <th>Birthdate</th>
    </tr>
    </thead>
    <tbody>
    @foreach($employees as $employee)
        <tr>
            <td>{{ $employee->firstname.' '.$employee->middlename.' '.$employee->lastname }}</td>
            <td>{{ $employee->date_hired }}</td>
            <td>{{ $employee->address }}</td>
            <td>{{ $employee->age }}</td>
            <td>{{ $employee->birthdate }}</td>
        </tr>
    @endforeach
    </tbody>
</table>