<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <style>
        table{border: 1px solid black; width: 100%;}
        table tr td{border: 1px solid black;}
        table tr th{background: #dee2e6; border: 1px solid black; font-weight: bold; text-align: center;}
    </style>
  </head>
  <body>
   <table style="border: 1px solid black;" cellpadding="3" cellspacing="0">
    <thead>
    <tr>
        <th>Name</th>
        <th>Date Hired</th>
        <th>Address</th>
        <th>Age</th>
        <th>Birthdate</th>
    </tr>
    </thead>
    <tbody>
    @foreach($employees as $employee)
        <tr>
            <td>{{ $employee->firstname.' '.$employee->middlename.' '.$employee->lastname }}</td>
            <td>{{ $employee->date_hired }}</td>
            <td>{{ $employee->address }}</td>
            <td>{{ $employee->age }}</td>
            <td>{{ $employee->birthdate }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
  </body>
</html>