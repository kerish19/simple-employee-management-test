<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <style>
        table{border: 1px solid black; width: 100%;}
        table tr td{border: 1px solid black;}
        table tr th{background: #dee2e6; border: 1px solid black; font-weight: bold; text-align: center;}
    </style>
  </head>
  <body>
   <table style="border: 1px solid black;" cellpadding="3" cellspacing="0">
    <thead>
    <tr>
        <th>Employee</th>
        <th>Salary</th>
    </tr>
    </thead>
    <tbody>
    @foreach($employees as $employee)
        <tr>
            <td>{{ $employee->name }}</td>
            <td>{{ $employee->salary }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
  </body>
</html>