<table>
    <thead>
    <tr>
        <th>Employee</th>
        <th>Salary</th>
    </tr>
    </thead>
    <tbody>
    @foreach($employees as $employee)
        <tr>
            <td>{{ $employee->name }}</td>
            <td>{{ $employee->salary }}</td>
        </tr>
    @endforeach
    </tbody>
</table>