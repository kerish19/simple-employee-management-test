<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Simple Employee Management - Login</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
  </head>

  <body class="text-center">

     <form class="form-signin" action="{{ url('login') }}" method="POST">
     
     @if (Session::has('msg'))
        <div class="alert alert-danger">
                <p>{!! Session::get('msg') !!}</p>
        </div>
      @endif
  
     {{ csrf_field() }}
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
      <input type="text" name="email" id="inputEmail" class="form-control" placeholder="Username/Email address" required autofocus>
      <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
  </body>
</html>