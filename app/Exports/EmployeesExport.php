<?php
namespace App\Exports;

use Illuminate\Contracts\Support\Responsable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Employees;

class EmployeesExport implements FromView, Responsable, ShouldAutoSize, WithEvents
{
    use Exportable;
    private $fileName = 'employees.xlsx';
    protected $datafroms = null; 
    protected $datatos = null; 
    public function __construct($datafrom, $datato)
    {
        $this->datafroms = $datafrom;
        $this->datatos = $datato;
    }
    public function view(): View
    {
        return view('exports.employees', [
            'employees' => Employees::where('date_hired', '>=', $this->datafroms)
                           ->where('date_hired', '<=', $this->datatos)
                           ->get()
        ]);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class  => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                
                
                $stylehead = [
                    'fill' => [
                           'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                           'color'    => ['argb' => 'FFe7c16a'],
                    ],
                ];
                
                $styleArray = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => '000000'],
                        ],
                    ],
                ];

                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($stylehead);
                
                foreach (range('A', 'Z') as $letter)
                {
                    for($i=1; $i<=100; $i++)
                    {
                         $event->sheet->getDelegate()->getStyle("$letter"."$i")->applyFromArray($styleArray);
                        $event->sheet->getDelegate()->getStyle("$letter"."$i")->getFont()->setSize(12);
                    }
                }
               
            },
        ];
    }
}