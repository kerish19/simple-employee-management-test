<?php
namespace App\Exports;

use Illuminate\Contracts\Support\Responsable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use App\EmployeesSalary;
use DB;

class SalaryExport implements FromView, Responsable, ShouldAutoSize, WithEvents
{
    use Exportable;
    private $fileName = 'employees_salary.xlsx';
 
    public function view(): View
    {
        return view('exports.employeesSalary', [
            'employees' => DB::table('employee_salary')->leftJoin('employees', 'employee_salary.employee', '=', 'employees.id')
        ->whereNULL('employee_salary.deleted_at')
        ->select('employee_salary.*',DB::raw("concat_ws(' ',employees.firstname,employees.middlename,employees.lastname) as name"))
        ->orderBy('salary', 'DESC')->get()
        ]);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class  => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                
                
                $stylehead = [
                    'fill' => [
                           'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                           'color'    => ['argb' => 'FFe7c16a'],
                    ],
                ];
                
                $styleArray = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => '000000'],
                        ],
                    ],
                ];

                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($stylehead);
                
                foreach (range('A', 'Z') as $letter)
                {
                    for($i=1; $i<=100; $i++)
                    {
                         $event->sheet->getDelegate()->getStyle("$letter"."$i")->applyFromArray($styleArray);
                        $event->sheet->getDelegate()->getStyle("$letter"."$i")->getFont()->setSize(12);
                    }
                }
               
            },
        ];
    }
}