<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeesSalary extends Model 
{

    protected $primaryKey = 'id';
    protected $table = 'employee_salary';
    protected $fillable = array(
        'employee',
        'salary'
    );

}