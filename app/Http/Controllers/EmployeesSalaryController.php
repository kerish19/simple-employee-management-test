<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use DB;
use App\Employees;
use App\EmployeesSalary;

class EmployeesSalaryController extends Controller
{
    protected $user;
    public function __construct()
    {
        // get the current logged in user
        $this->middleware(function ($request, $next)
        {
        $this->user = Auth::user();
        $this->user = $this->user->firstname.' '.$this->user->lastname;
        return $next($request);
        });
    }
    
    // List all emloyees from the database
    public function index(Request $request)
    {
        if($request->isMethod('post')):
            $empname = $request->input('employee');
            $salary = $request->input('salary');
            $employees = DB::table('employee_salary')->leftJoin('employees', 'employee_salary.employee', '=', 'employees.id')
            ->whereNULL('employee_salary.deleted_at');
            if(!empty($empname)):
                $employees->where(function($query) use ($empname)
                {
                    $query->where('employees.firstname', 'like', '%' . $empname . '%')
                        ->Orwhere('employees.middlename', 'like', '%' . $empname . '%')
                        ->Orwhere('employees.lastname', 'like', '%' . $empname . '%');
                });
            endif;
            
            if(!empty($salary)):
                $employees->where('salary', 'like', '%' . $salary . '%');
            endif;
            
            $employees = $employees->select('employee_salary.*',DB::raw("concat_ws(' ',employees.firstname,employees.middlename,employees.lastname) as name"))
            ->paginate(10);
        else:
            $employees = DB::table('employee_salary')
            ->leftJoin('employees', 'employee_salary.employee', '=', 'employees.id')
            ->whereNULL('employee_salary.deleted_at')
            ->select('employee_salary.*',DB::raw("concat_ws(' ',employees.firstname,employees.middlename,employees.lastname) as name"))
            ->paginate(10);
        endif;
        
        $user = $this->user;
        return view('employees.salarylist', compact('user'))->with('employees', $employees);
    }
    
    
    // Show add employee form
    public function create()
    {
        $employee = new EmployeesSalary;
        $employees = Employees::all();
        $data = array('user' => $this->user, 'title'=>'Add New Record', 'employee' => $employee, 'employees' => $employees);
        return view('employees.salaryform', $data)->with('id','');
    }
    
    
    // Show edit form with populated data from database based on id
    public function show($id)
    {
        $employee = EmployeesSalary::find($id);
        $employees = Employees::all();
        $data = array('user' => $this->user,'title' => 'Employee', 'employee' => $employee, 'employees' => $employees);
        return view('employees.salaryform', $data)->with('id',$id);
    }
    
    
    // Insert new employee record
    public function store(Request $request)
    {
        $inputs = $request->except(['_token', 'add_btn']);
        $employee = new EmployeesSalary;
        foreach($inputs as $input => $val):
            $employee->$input = $val;
        endforeach;
        $employee->save();
        return redirect()->back()->with('msg', 'Record successfuly saved!');
    }
    
    
    // Update current employee details
    public function update(Request $request, $id)
    {
        $employee = EmployeesSalary::find($id);
        $inputs = $request->except(['_token', 'add_btn', '_method']);
        foreach($inputs as $input => $val):
            $employee->$input = $val;
        endforeach;
        $employee->save();
        return redirect()->back()->with('msg', 'Record successfuly updated!');
    }
    
}