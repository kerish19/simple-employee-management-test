<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use DB;
use App\City;
use App\State;

class CityController extends Controller
{
    protected $user;
    public function __construct()
    {
        // get the current logged in user
        $this->middleware(function ($request, $next)
        {
        $this->user = Auth::user();
        $this->user = $this->user->firstname.' '.$this->user->lastname;
        return $next($request);
        });
    }
    
    public function index(Request $request)
    {
        if($request->isMethod('post')):
            $city = $request->input('city');
            $state = $request->input('state');
            $dep = DB::table('city')->leftJoin('state', 'city.state', '=', 'state.id');
            
                if(!empty($state)):
                    $dep->where('state.name', 'like', '%' . $state . '%');
                endif;
                if(!empty($city)):
                    $dep->where('city.name', 'like', '%' . $city . '%');
                endif;
            
            $dep = $dep->select('city.*','state.name as statename')->paginate(10);
            
        else:
            $dep = DB::table('city')
            ->leftJoin('state', 'city.state', '=', 'state.id')
            ->select('city.*','state.name as statename')
            ->paginate(10);
        endif;
        
        $user = $this->user;
        return view('system.city', compact('user'))->with('dep', $dep);
    }
    
    // Show add form
    public function create()
    {
        $info = new City;
        $country = State::all();
        $data = array('user' => $this->user, 'title'=>'Add New City', 'info' => $info, 'states' => $country);
        return view('system.city_form', $data)->with('id','');
    }
    
    // Show edit form with populated data from database based on id
    public function show($id)
    {
        $info = City::find($id);
        $country = State::all();
        $data = array('user' => $this->user,'title' => 'City: '.$info->name, 'info' => $info, 'states' => $country);
        return view('system.city_form', $data)->with('id',$id);
    }
    
    
    // Insert new record
    public function store(Request $request)
    {
        $inputs = $request->except(['_token', 'add_btn']);
        $info = new City;
        foreach($inputs as $input => $val):
                $info->$input = $val;
        endforeach;
        $info->save();
        return redirect()->back()->with('msg', 'Record successfuly saved!');
    }
    
    
    // Update current details
    public function update(Request $request, $id)
    {
        $info = City::find($id);
        $inputs = $request->except(['_token', 'add_btn', '_method']);
        foreach($inputs as $input => $val):
            $info->$input = $val;
        endforeach;
        $info->save();
        return redirect()->back()->with('msg', 'Record successfuly saved!');
    }
    
    public function destroy($id)
    {
        if(!empty($id)):
            $delete = City::where('id',$id)->delete();
             return "Record successfuly deleted";
        else:
            return "There is no specific id selected. Contact administrator.";
        endif;
    }
    
    
}