<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use DB;
use App\State;
use App\Country;

class StateController extends Controller
{
    protected $user;
    public function __construct()
    {
        // get the current logged in user
        $this->middleware(function ($request, $next)
        {
        $this->user = Auth::user();
        $this->user = $this->user->firstname.' '.$this->user->lastname;
        return $next($request);
        });
    }
    
    public function index(Request $request)
    {
        if($request->isMethod('post')):
            $country = $request->input('country');
            $state = $request->input('state');
            $dep = DB::table('state')->leftJoin('country', 'state.country', '=', 'country.id');
                
                if(!empty($state)):
                    $dep->where('state.name', 'like', '%' . $state . '%');
                endif;
                if(!empty($country)):
                    $dep->where('country.name', 'like', '%' . $country . '%');
                endif;
                
            $dep = $dep->select('state.*','country.name as countryname')->paginate(10);
        else:
            $dep = DB::table('state')
            ->leftJoin('country', 'state.country', '=', 'country.id')
            ->select('state.*','country.name as countryname')
            ->paginate(10);
        endif;
        $user = $this->user;
        return view('system.state', compact('user'))->with('dep', $dep);
    }
    
    // Show add form
    public function create()
    {
        $info = new State;
        $country = Country::all();
        $data = array('user' => $this->user, 'title'=>'Add New State', 'info' => $info, 'countries' => $country);
        return view('system.state_form', $data)->with('id','');
    }
    
    // Show edit form with populated data from database based on id
    public function show($id)
    {
        $info = State::find($id);
        $country = Country::all();
        $data = array('user' => $this->user,'title' => 'State: '.$info->name, 'info' => $info, 'countries' => $country);
        return view('system.state_form', $data)->with('id',$id);
    }
    
    
    // Insert new record
    public function store(Request $request)
    {
        $inputs = $request->except(['_token', 'add_btn']);
        $info = new State;
        foreach($inputs as $input => $val):
                $info->$input = $val;
        endforeach;
        $info->save();
        return redirect()->back()->with('msg', 'Record successfuly saved!');
    }
    
    
    // Update current details
    public function update(Request $request, $id)
    {
        $info = State::find($id);
        $inputs = $request->except(['_token', 'add_btn', '_method']);
        foreach($inputs as $input => $val):
            $info->$input = $val;
        endforeach;
        $info->save();
        return redirect()->back()->with('msg', 'Record successfuly saved!');
    }
    
    public function destroy($id)
    {
        if(!empty($id)):
            $delete = State::where('id',$id)->delete();
             return "Record successfuly deleted";
        else:
            return "There is no specific id selected. Contact administrator.";
        endif;
    }
    
    
}