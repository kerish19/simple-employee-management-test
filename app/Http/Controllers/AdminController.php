<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use DB;

class AdminController extends Controller
{
    protected $user;
    public function __construct()
    {
        $this->middleware(function ($request, $next)
        {
        $this->user = Auth::user();
        $this->user = $this->user->firstname.' '.$this->user->lastname;
        return $next($request);
        });
    }
    
    public function logout()
    {
         Auth::logout();
         return redirect('/')->with('msg', "You have successfuly logged out to your account.");
    }
    
    public function dashboard()
    {
        $user = $this->user;
        return view('dashboard', compact('user'));
    }
    
    public function deleteRecords(Request $request)
    {
        $date = date('Y-m-d h:i:s a', time());
        $update = DB::table($request->input('table'))->where('id', $request->input('id'))->update(['deleted_at' => $date]);
        return 'Record Deleted!';
    }
    
    public function reports()
    {
        $user = $this->user;
        return view('reports', compact('user'));
    }

    
}