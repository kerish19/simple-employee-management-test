<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use DB;
use App\Division;

class DivisionController extends Controller
{
    protected $user;
    public function __construct()
    {
        // get the current logged in user
        $this->middleware(function ($request, $next)
        {
        $this->user = Auth::user();
        $this->user = $this->user->firstname.' '.$this->user->lastname;
        return $next($request);
        });
    }
    
    public function index(Request $request)
    {
        if($request->isMethod('post')):
            $division = $request->input('division');
            $dep = DB::table('division');
                if(!empty($division)):
                    $dep->where('name', 'like', '%' . $division . '%');
                endif;
            $dep = $dep->paginate(10);
        else:
            $dep = DB::table('division')->paginate(10);
        endif;
        
        $user = $this->user;
        return view('system.division', compact('user'))->with('dep', $dep);
    }
    
    // Show add form
    public function create()
    {
        $info = new Division;
        $data = array('user' => $this->user, 'title'=>'Add New Division', 'info' => $info);
        return view('system.division_form', $data)->with('id','');
    }
    
    // Show edit form with populated data from database based on id
    public function show($id)
    {
        $info = Division::find($id);
        $data = array('user' => $this->user,'title' => 'Division: '.$info->name, 'info' => $info);
        return view('system.division_form', $data)->with('id',$id);
    }
    
    
    // Insert new record
    public function store(Request $request)
    {
        $inputs = $request->except(['_token', 'add_btn']);
        $info = new Division;
        foreach($inputs as $input => $val):
                $info->$input = $val;
        endforeach;
        $info->save();
        return redirect()->back()->with('msg', 'Record successfuly saved!');
    }
    
    
    // Update current details
    public function update(Request $request, $id)
    {
        $info = Division::find($id);
        $inputs = $request->except(['_token', 'add_btn', '_method']);
        foreach($inputs as $input => $val):
            $info->$input = $val;
        endforeach;
        $info->save();
        return redirect()->back()->with('msg', 'Record successfuly saved!');
    }
    
    public function destroy($id)
    {
        if(!empty($id)):
            $delete = Division::where('id',$id)->delete();
             return "Record successfuly deleted";
        else:
            return "There is no specific id selected. Contact administrator.";
        endif;
    }
    
    
}