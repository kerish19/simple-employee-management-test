<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use Redirect;
use DB;
use App\Employees;
use App\City;
use App\State;
use App\Country;
use App\User;

class UsersController extends Controller
{
    protected $user;
    public function __construct()
    {
        // get the current logged in user
        $this->middleware(function ($request, $next)
        {
        $this->user = Auth::user();
        $this->user = $this->user->firstname.' '.$this->user->lastname;
        return $next($request);
        });
    }
    
    public function index(Request $request)
    {
        if($request->isMethod('post')):
            $fname = $request->input('firstname');
            $lname = $request->input('lastname');
            $uname = $request->input('email');
            $users = DB::table('users');
            if(!empty($fname)):
                $users->where('firstname', 'like', '%' . $fname . '%');
            endif;
            if(!empty($lname)):
                $users->where('lastname', 'like', '%' . $lname . '%');
            endif;
            if(!empty($uname)):
                $users->where('email', 'like', '%' . $uname . '%');
            endif;
            $users = $users->paginate(10);
        else:
            $users = DB::table('users')->paginate(10);
        endif;
        
        $user = $this->user;
        
        return view('users.list', compact('user'))->with('users', $users);
    }
    
    // Show add user form
    public function create()
    {
        $user = new User;
        $data = array('user' => $this->user, 'title'=>'Add New User', 'use' => $user);
        return view('users.form', $data)->with('id','');
    }
    
    // Show edit form with populated data from database based on id
    public function show($id)
    {
        $user = User::find($id);
        $data = array('user' => $this->user,'title' => 'User: '.$user->firstname.' '.$user->lastname, 'use' => $user);
        return view('users.form', $data)->with('id',$id);
    }
    
    
    // Insert new user record
    public function store(Request $request)
    {
        $inputs = $request->except(['_token', 'add_btn']);
        $user = new User;
        foreach($inputs as $input => $val):
            if($input=='password'):
                $user->$input = Hash::make($val);
            else:
                $user->$input = $val;
            endif;
        endforeach;
        $user->save();
        return redirect()->back()->with('msg', 'Record successfuly saved!');
    }
    
    
    // Update current employee details
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $inputs = $request->except(['_token', 'add_btn', '_method']);
        foreach($inputs as $input => $val):
            $user->$input = $val;
        endforeach;
        $user->save();
        return redirect()->back()->with('msg', 'Record successfuly saved!');
    }
    
    // update user password
    public function updatePassword(Request $request)
    {
      $user_id = $request->input('id'); 
      $obj_user = User::find($user_id);
      $current_password = $obj_user->password;
      
      if(Hash::check($request->input('oldpass'), $current_password)):
      
        if($request->input('password')==$request->input('password2')):                     
            $obj_user->password = Hash::make($request->input('password'));
            $obj_user->save(); 
            return redirect()->back()->with('msg-success', 'Password successfully updated!');
        else:
            return redirect()->back()->with('msg-error', 'Password not matched.');
        endif;
        
      else:
            return redirect()->back()->with('msg-error', 'Current Password not correct.');
      endif;
      
    }
    
}