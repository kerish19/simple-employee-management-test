<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Auth;
use Redirect;

class LoginController extends Controller
{

    use ThrottlesLogins;
    
    public function __construct()
    {
        
    }
    
    public function viewForm()
    {
        return view('login');
    }
    
    public function doLogin(Request $request)
    {
       
            // create our user data for the authentication
            $userdata = array(
                'email'     => $request->input('email'),
                'password'  => $request->input('password')
            );
            
            if ($this->hasTooManyLoginAttempts($request)) 
            {
                $this->fireLockoutEvent($request);
                return redirect()->back()->with('msg', 'Too many login attempts. Please try again after 5 minutes.');
            }else{
                // attempt to do the login
                if (Auth::attempt($userdata))
                {
                    $this->clearLoginAttempts($request);
                    return redirect('admin/dashboard');
                } else {        
                    $this->incrementLoginAttempts($request);
                    return redirect()->back()->with('msg', 'Invalid credentials!'); 
                }
            }
        
    }
    
     public function username()
    {
        return 'email';
    }
    
     protected function hasTooManyLoginAttempts(Request $request)
    {
       $maxLoginAttempts = 3;
    
       $lockoutTime = 5; // In minutes
    
       return $this->limiter()->tooManyAttempts(
           $this->throttleKey($request), $maxLoginAttempts, $lockoutTime
       );
    }    
    
}
