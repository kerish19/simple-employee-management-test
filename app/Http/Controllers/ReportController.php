<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use DB;
use PDF;
use App\Exports\EmployeesExport;
use App\Exports\SalaryExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Employees;
use App\EmployeesSalary;

class ReportController extends Controller
{
    public function hiredExcel(Request $request)
    {
        $from = $request->input('datefrom');
        $to = $request->input('dateto');
        return new EmployeesExport($from, $to);
    }
    
    public function hiredPDF(Request $request)
    {
        $from = $request->input('datefrom');
        $to = $request->input('dateto');
        $employees = Employees::where('date_hired', '>=', $from)
                           ->where('date_hired', '<=', $to)
                           ->get();
        $pdf = PDF::loadView('exports.employeepdf', compact('employees'));
        return $pdf->download('employees.pdf');
    }
    
    
    public function salaryExcel(Request $request)
    {
        return new SalaryExport();
    }
    
    public function salaryPDF(Request $request)
    {
        $employees = DB::table('employee_salary')->leftJoin('employees', 'employee_salary.employee', '=', 'employees.id')
        ->whereNULL('employee_salary.deleted_at')
        ->select('employee_salary.*',DB::raw("concat_ws(' ',employees.firstname,employees.middlename,employees.lastname) as name"))
        ->orderBy('salary', 'DESC')->get();
        
        $pdf = PDF::loadView('exports.employeesSalarypdf', compact('employees'));
        return $pdf->download('employees_salary.pdf');
    }
}