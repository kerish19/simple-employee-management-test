<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use DB;
use App\Employees;
use App\City;
use App\State;
use App\Country;

class EmployeeController extends Controller
{
    protected $user;
    public function __construct()
    {
        // get the current logged in user
        $this->middleware(function ($request, $next)
        {
        $this->user = Auth::user();
        $this->user = $this->user->firstname.' '.$this->user->lastname;
        return $next($request);
        });
    }
    
    // List all emloyees from the database
    public function index(Request $request)
    {
        if($request->isMethod('post')):
        $empname = $request->input('employee');
        $department = $request->input('department');
        
        $employees = DB::table('employees')
        ->leftJoin('department', 'employees.department', '=', 'department.id')
        ->whereNULL('employees.deleted_at');
        
            if(!empty($empname)):
                $employees->where(function($query) use ($empname)
                {
                    $query->where('employees.firstname', 'like', '%' . $empname . '%')
                        ->Orwhere('employees.middlename', 'like', '%' . $empname . '%')
                        ->Orwhere('employees.lastname', 'like', '%' . $empname . '%');
                });
            endif;
            
            if(!empty($department)):
                $employees->where('department.name', 'like', '%' . $department . '%');
            endif;
            
          $employees = $employees->select('employees.*','department.name')->paginate(10);
        
        else:
        
        $employees = DB::table('employees')
        ->leftJoin('department', 'employees.department', '=', 'department.id')
        ->whereNULL('employees.deleted_at')
        ->select('employees.*','department.name')
        ->paginate(10);
        
        endif;
        
        $user = $this->user;
        return view('employees.list', compact('user'))->with('employees', $employees);
    }
    
    
    // Show add employee form
    public function create()
    {
        $employee = new Employees;
        $cities = City::all();
        $states = State::all();
        $country = Country::all();
        $divisions = DB::table('division')->get();
        $departments = DB::table('department')->get();
        $data = array('user' => $this->user, 'title'=>'Add New Employee', 'employee' => $employee, 'cities' => $cities, 'states' => $states, 'countries' => $country, 'departments' => $departments, 'divisions' => $divisions);
        return view('employees.form', $data)->with('id','');
    }
    
    
    // Show edit form with populated data from database based on id
    public function show($id)
    {
        $employee = Employees::find($id);
        $cities = City::all();
        $states = State::all();
        $country = Country::all();
        $divisions = DB::table('division')->get();
        $departments = DB::table('department')->get();
        $data = array('user' => $this->user,'title' => 'Employee: '.$employee->firstname.' '.$employee->lastname, 'employee' => $employee, 'cities' => $cities, 'states' => $states, 'countries' => $country, 'departments' => $departments, 'divisions' => $divisions);
        return view('employees.form', $data)->with('id',$id);
    }
    
    
    // Insert new employee record
    public function store(Request $request)
    {
        $inputs = $request->except(['_token', 'add_employee_btn']);
        $employee = new Employees;
        foreach($inputs as $input => $val):
            $employee->$input = $val;
        endforeach;
        
        // Handle File Upload
        if($request->hasFile('picture')) 
        {
            $filenameWithExt = $request->file('picture')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);            
           // Get just ext
            $extension = $request->file('picture')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;                       
          // Upload Image
            $path = $request->file('picture')->storeAs('/', $fileNameToStore);
        } else {
            $fileNameToStore = '';
        }
        
        $employee->picture = $fileNameToStore;
        $employee->save();
        return redirect()->back()->with('msg', 'Record successfuly saved!');
    }
    
    
    // Update current employee details
    public function update(Request $request, $id)
    {
        $employee = Employees::find($id);
        $inputs = $request->except(['_token', 'add_employee_btn', '_method']);
        foreach($inputs as $input => $val):
            $employee->$input = $val;
        endforeach;
        if($request->hasFile('picture')) 
        {
            $filenameWithExt = $request->file('picture')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);            
           // Get just ext
            $extension = $request->file('picture')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;                       
          // Upload Image
            $path = $request->file('picture')->storeAs('/', $fileNameToStore);
        } else {
            $fileNameToStore = '';
        }
        
        $employee->picture = $fileNameToStore;
        $employee->save();
        return redirect()->back()->with('msg', 'Record successfuly updated!');
    }
    
}