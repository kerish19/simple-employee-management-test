<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use DB;
use App\Country;

class CountryController extends Controller
{
    protected $user;
    public function __construct()
    {
        // get the current logged in user
        $this->middleware(function ($request, $next)
        {
        $this->user = Auth::user();
        $this->user = $this->user->firstname.' '.$this->user->lastname;
        return $next($request);
        });
    }
    
    public function index(Request $request)
    {
        if($request->isMethod('post')):
            $country = $request->input('country');
            $code = $request->input('code');
            $dep = DB::table('country');
                if(!empty($country)):
                    $dep->where('name', 'like', '%' . $country. '%');
                endif;
                if(!empty($code)):
                    $dep->where('country_code', 'like', '%' . $code. '%');
                endif;
            $dep = $dep->paginate(10);
        else:
            $dep = DB::table('country')->paginate(10);
        endif;
        
        $user = $this->user;
        return view('system.country', compact('user'))->with('dep', $dep);
    }
    
    // Show add form
    public function create()
    {
        $info = new Country;
        $data = array('user' => $this->user, 'title'=>'Add New Country', 'info' => $info);
        return view('system.country_form', $data)->with('id','');
    }
    
    // Show edit form with populated data from database based on id
    public function show($id)
    {
        $info = Country::find($id);
        $data = array('user' => $this->user,'title' => 'Country: '.$info->name, 'info' => $info);
        return view('system.country_form', $data)->with('id',$id);
    }
    
    
    // Insert new record
    public function store(Request $request)
    {
        $inputs = $request->except(['_token', 'add_btn']);
        $info = new Country;
        foreach($inputs as $input => $val):
                $info->$input = $val;
        endforeach;
        $info->save();
        return redirect()->back()->with('msg', 'Record successfuly saved!');
    }
    
    
    // Update current details
    public function update(Request $request, $id)
    {
        $info = Country::find($id);
        $inputs = $request->except(['_token', 'add_btn', '_method']);
        foreach($inputs as $input => $val):
            $info->$input = $val;
        endforeach;
        $info->save();
        return redirect()->back()->with('msg', 'Record successfuly saved!');
    }
    
    public function destroy($id)
    {
        if(!empty($id)):
            $delete = Country::where('id',$id)->delete();
             return "Record successfuly deleted";
        else:
            return "There is no specific id selected. Contact administrator.";
        endif;
    }
    
    
}