<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use DB;
use App\Department;

class DepartmentController extends Controller
{
    protected $user;
    public function __construct()
    {
        // get the current logged in user
        $this->middleware(function ($request, $next)
        {
        $this->user = Auth::user();
        $this->user = $this->user->firstname.' '.$this->user->lastname;
        return $next($request);
        });
    }
    
    public function index(Request $request)
    {
        if($request->isMethod('post')):
            $department = $request->input('department');
            $dep = DB::table('department');
                if(!empty($department)):
                    $dep->where('name', 'like', '%' . $department . '%');
                endif;
            $dep = $dep->paginate(10);
        else:
            $dep = DB::table('department')->paginate(10);
        endif;
        
        $user = $this->user;
        return view('system.department', compact('user'))->with('dep', $dep);
    }
    
    // Show add form
    public function create()
    {
        $info = new Department;
        $data = array('user' => $this->user, 'title'=>'Add New Department', 'info' => $info);
        return view('system.department_form', $data)->with('id','');
    }
    
    // Show edit form with populated data from database based on id
    public function show($id)
    {
        $info = Department::find($id);
        $data = array('user' => $this->user,'title' => 'Department: '.$info->name, 'info' => $info);
        return view('system.department_form', $data)->with('id',$id);
    }
    
    
    // Insert new record
    public function store(Request $request)
    {
        $inputs = $request->except(['_token', 'add_btn']);
        $info = new Department;
        foreach($inputs as $input => $val):
                $info->$input = $val;
        endforeach;
        $info->save();
        return redirect()->back()->with('msg', 'Record successfuly saved!');
    }
    
    
    // Update current details
    public function update(Request $request, $id)
    {
        $info = Department::find($id);
        $inputs = $request->except(['_token', 'add_btn', '_method']);
        foreach($inputs as $input => $val):
            $info->$input = $val;
        endforeach;
        $info->save();
        return redirect()->back()->with('msg', 'Record successfuly saved!');
    }
    
    public function destroy($id)
    {
        if(!empty($id)):
            $delete = Department::where('id',$id)->delete();
             return "Record successfuly deleted";
        else:
            return "There is no specific id selected. Contact administrator.";
        endif;
    }
    
    
}