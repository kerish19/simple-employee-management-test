<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model 
{

    protected $primaryKey = 'id';
    protected $table = 'employees';
    protected $fillable = array(
        'firstname',
        'lastname'
    );

}